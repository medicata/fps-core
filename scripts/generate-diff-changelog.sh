#!/usr/bin/env bash

# Внимание! Перед запуском скрипта:
# 1. Запустить docker-контейнер командой:
# docker run -d --name "postgres-medicata-dev" -p 5438:5432 docker.it-aces.com:5000/postgres-medicata-dev
# 2. Запустить тест GenerateDb

script_path=`realpath $0`
cd `dirname ${script_path}`
pwd

./liquibase --changeLogFile=../src/main/resources/db/changelog/db.changelog-drop-extensions-always.xml migrate

./liquibase dropAll

# Так как существующая версия liquibase не удаляет объекты из схем кроме public
# сделан этакий "костыль" (запускаем из под mingw(git-bash))
# TODO - для запуска под linux проверять uname
# winpty docker exec -it "postgres-medicata-dev" psql -U medicata -d medicata_liq -c "DROP SCHEMA pharmacy CASCADE;"
# winpty docker exec -it "postgres-medicata-dev" psql -U medicata -d medicata_liq -c "DROP SCHEMA patient_api CASCADE;"

./liquibase --changeLogFile=../src/main/resources/db/changelog/db.changelog-master.xml migrate

rm -fv db.diff-changelog.xml

./liquibase \
        --changeLogFile=db.diff-changelog.xml \
    diffChangeLog \
        --schemas=public \
        --referenceUrl="jdbc:postgresql://docker.host.ip:5437/fps_gen" \
        --referenceUsername=medicata \
        --referencePassword=medicata \
        --includeSchema=true

sed -i 's/type="TIMESTAMP WITHOUT TIME ZONE"/type="DATETIME"/g' db.diff-changelog.xml
sed -i 's/type="SMALLINT(5)"/type="SMALLINT"/g' db.diff-changelog.xml
sed -i 's/defaultValueComputed="now()"/defaultValueComputed="CURRENT_TIMESTAMP"/g' db.diff-changelog.xml
