package com.itaces.medicata.fps.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.session.web.http.HeaderHttpSessionIdResolver;
import org.springframework.session.web.http.HttpSessionIdResolver;

/**
 * Конфигуратор Http-сессий
 */
@Configuration
public class HttpSessionConfig {

    public static final String HEADER_X_AUTH_TOKEN = "X-Auth-Token";
    public static final String HEADER_X_CLIENT_TOKEN = "X-Auth-Client-Token";

    /**
     * Конфигурация возврата id сессии
     *
     * @return - ресолвер
     */
    @Bean
    public HttpSessionIdResolver httpSessionIdResolver() {
        return HeaderHttpSessionIdResolver.xAuthToken();
    }
}
