package com.itaces.medicata.fps.config;

import com.itaces.medicata.fps.controller.AuthController;
import com.itaces.medicata.fps.controller.ClientController;
import com.itaces.medicata.fps.controller.admin.AdminAuthController;
import com.itaces.medicata.fps.controller.admin.AdminController;
import com.itaces.medicata.fps.security.*;
import com.itaces.medicata.fps.util.LogsUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.authentication.session.SessionFixationProtectionStrategy;
import org.springframework.security.web.util.matcher.AndRequestMatcher;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsUtils;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

import javax.servlet.http.HttpServletResponse;

/**
 * Конфигуратор системы безопасности Web-запросов
 */
@Slf4j
@Configuration
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    public static final String FPS_USER = "FPS_USER";
    public static final String FPS_ADMIN = "FPS_ADMIN";
    public static final String ROOT_ALL = "/**";

    @Autowired
    private UserAccountAuthenticationProvider userAccountAuthenticationProvider;

    @Autowired
    private AdminAuthenticationProvider adminAuthenticationProvider;

    /**
     * Фильтр для логина admin
     *
     * @return - фильтр
     * @throws Exception - всё плохо
     */
    @Bean
    public AdminAccountAuthenticationFilter adminAccountAuthenticationFilter() throws Exception {
        final RequestMatcher requestMatcher = new AndRequestMatcher(new AntPathRequestMatcher(AdminAuthController.pathLogin,
                                                                                              HttpMethod.GET.name()));

        final AdminAccountAuthenticationFilter adminAccountAuthenticationFilter = new AdminAccountAuthenticationFilter(
                requestMatcher);

        adminAccountAuthenticationFilter.setAuthenticationManager(authenticationManager());
        adminAccountAuthenticationFilter.setSessionAuthenticationStrategy(new SessionFixationProtectionStrategy());

        return adminAccountAuthenticationFilter;
    }

    @Override
    public void configure(WebSecurity web) {
        web.ignoring().antMatchers(
                "/v2/api-docs",
                rootAll("/swagger-resources"),
                "/swagger-ui.html",
                rootAll("/webjars"),
                rootAll("/images")
        );
    }

    /**
     * Фильтр CORS
     *
     * @return - фильтр
     */
    @Bean
    public FilterRegistrationBean<?> corsFilter() {
        final UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        final CorsConfiguration config = new CorsConfiguration();
        config.setAllowCredentials(true);
        config.addAllowedOrigin(CorsConfiguration.ALL);

        config.addAllowedHeader(CorsConfiguration.ALL);
        config.addAllowedMethod(CorsConfiguration.ALL);
        source.registerCorsConfiguration(ROOT_ALL, config);

        final FilterRegistrationBean<CorsFilter> bean = new FilterRegistrationBean<>(new CorsFilter(source));

        bean.setOrder(Ordered.HIGHEST_PRECEDENCE);

        return bean;
    }

    /**
     * Инициализация шифрователя паролей
     *
     * @return - шифрователь
     */
    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    /**
     * Фильтр для логина
     *
     * @return - фильтр
     * @throws Exception - всё плохо
     */
    @Bean
    public UserAccountAuthenticationFilter userAccountAuthenticationFilter() throws Exception {
        final RequestMatcher requestMatcher = new AndRequestMatcher(new AntPathRequestMatcher(AuthController.pathLogin,
                                                                                              HttpMethod.GET.name()));

        final UserAccountAuthenticationFilter userAccountAuthenticationFilter = new UserAccountAuthenticationFilter(
                requestMatcher);

        userAccountAuthenticationFilter.setAuthenticationManager(authenticationManager());
        userAccountAuthenticationFilter.setSessionAuthenticationStrategy(new SessionFixationProtectionStrategy());

        return userAccountAuthenticationFilter;
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) {
        auth.authenticationProvider(this.userAccountAuthenticationProvider);
        auth.authenticationProvider(this.adminAuthenticationProvider);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.IF_REQUIRED)
                .sessionFixation()
                .newSession()

                .and()

                .csrf().disable()

                //.anonymous().disable()
                .logout()
                .logoutSuccessHandler(new DoNothingLogoutSuccessHandler())
                .logoutRequestMatcher(new AntPathRequestMatcher("/*/auth/logout", HttpMethod.POST.name()))
                .invalidateHttpSession(true)

                .and()

                .authorizeRequests()

                .requestMatchers(CorsUtils::isPreFlightRequest)
                .permitAll()

                /*.antMatchers(
                        rootAll(AuthController.root))
                .hasAnyRole(PATIENT_API_ANONYMOUS_USER)
*/

                .antMatchers(rootAll(ClientController.root))
                .hasAnyRole(FPS_USER)

                .antMatchers(rootAll(AdminController.root))
                .hasRole(FPS_ADMIN)

                .and()

                // Настройка обработки ошибок
                .exceptionHandling()
                // Если проблемы при аутентифкации (прислали неверный или протухший токен), то возвращаем 401
                .authenticationEntryPoint((request, response, e) -> {
                    log.warn(LogsUtils.markerSpace("authenticationEntryPoint", "e.message"), e.getLocalizedMessage());
                    response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
                })
                .and()

                .addFilterAt(userAccountAuthenticationFilter(), UsernamePasswordAuthenticationFilter.class)
                .addFilterAfter(adminAccountAuthenticationFilter(), UserAccountAuthenticationFilter.class);
    }

    private static String rootAll(String root) {
        return root + ROOT_ALL;
    }
}