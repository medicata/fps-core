package com.itaces.medicata.fps.config;

import com.itaces.medicata.fps.controller.ClientController;
import com.itaces.medicata.fps.controller.admin.AdminController;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.hateoas.client.LinkDiscoverer;
import org.springframework.hateoas.client.LinkDiscoverers;
import org.springframework.hateoas.mediatype.collectionjson.CollectionJsonLinkDiscoverer;
import org.springframework.plugin.core.SimplePluginRegistry;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Конфигуратор Swagger2
 */
@EnableSwagger2
//@EnableSwagger2WebMvc
@Configuration
public class SwaggerConfig {
    @Value("${application.host}")
    private String host;

    /**
     * Конфигурация Swagger2
     *
     * @return - набор настроек для Swagger2
     */
    @Bean
    public Docket adminApi() {
        return new Docket(DocumentationType.SWAGGER_2).groupName("Admin API")
                .host(host)
                .globalOperationParameters(Collections.singletonList(
                        new ParameterBuilder().name(HttpSessionConfig.HEADER_X_AUTH_TOKEN)
                                .parameterType("header")
                                .modelRef(new ModelRef("String"))
                                .build()
//                        new ParameterBuilder().name(PatientApiVersionHandlerInterceptor.X_CURRENT_VERSION).parameterType("header").modelRef(new ModelRef("String")).build()
                ))
                .ignoredParameterTypes(HttpSession.class)
                .select()
                .apis(RequestHandlerSelectors.any())
                .paths(PathSelectors.ant(AdminController.root + "/**"))
                .build();
    }

    /**
     * Конфигурация Swagger2
     *
     * @return - набор настроек для Swagger2
     */
    @Bean
    public Docket clientApi() {
        return new Docket(DocumentationType.SWAGGER_2).groupName("Client API")

                .host(host)
                .globalOperationParameters(Collections.singletonList(
                        new ParameterBuilder().name(HttpSessionConfig.HEADER_X_AUTH_TOKEN)
                                .parameterType("header")
                                .modelRef(new ModelRef("String"))
                                .build()
//                        new ParameterBuilder().name(PatientApiVersionHandlerInterceptor.X_CURRENT_VERSION).parameterType("header").modelRef(new ModelRef("String")).build()
                ))
                .ignoredParameterTypes(HttpSession.class)
                .select()
                .apis(RequestHandlerSelectors.any())
                .paths(PathSelectors.ant(ClientController.root + "/**"))
                .build();
    }



    /**
     * Метод для hateoas
     * @return бин
     */
    @Bean
    public LinkDiscoverers discoverers() {
        List<LinkDiscoverer> plugins = new ArrayList<>();
        plugins.add(new CollectionJsonLinkDiscoverer());
        return new LinkDiscoverers(SimplePluginRegistry.create(plugins));

    }
}