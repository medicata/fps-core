package com.itaces.medicata.fps.config;

import com.itaces.medicata.fps.util.StringToUUIDConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.format.FormatterRegistry;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.multipart.MultipartResolver;
import org.springframework.web.multipart.support.StandardServletMultipartResolver;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * Конфигуратор WebMvc
 */
@EnableScheduling
@Configuration
public class WebMvcConfig implements WebMvcConfigurer {

    /**
     * Для форм
     *
     * @return резолвер
     */
    @Bean
    public MultipartResolver multipartResolver() {
        return new StandardServletMultipartResolver();
    }

    /**
     * Форматтер UUID
     *
     * @param registry регистер
     */
    @Override
    public void addFormatters(FormatterRegistry registry) {
        registry.addConverter(new StringToUUIDConverter());
    }

}
