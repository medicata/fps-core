package com.itaces.medicata.fps.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.time.ZonedDateTime;
import java.util.Date;

/**
 * Пользователь
 */
@ToString
@Getter
@Setter
@Table(name = "user_account")
@Entity
public class UserAccount extends UUIDIdentifiable {

    private String name;
    private String surname;
    private String patronymic;
    private Date birthday;

    @Column(name = "last_access")
    private ZonedDateTime lastAccessDateTime;

    @Column(name = "access_token")
    private String accessToken;
}
