package com.itaces.medicata.fps.util;

import org.springframework.core.convert.converter.Converter;
import org.springframework.util.StringUtils;

import java.util.UUID;

/**
 * Вспомогательный класс
 * для удобного объявления параметров контроллеров (@RequestParam UUID id)
 * встроенный в spring-core (5.0.9) класс org.springframework.core.convert.support.StringToUUIDConverter
 * непубличный...
 */
public class StringToUUIDConverter implements Converter<String, UUID> {

    @Override
    public UUID convert(String source) {
        return (StringUtils.hasLength(source) ? UUID.fromString(source.trim()) : null);
    }
}
