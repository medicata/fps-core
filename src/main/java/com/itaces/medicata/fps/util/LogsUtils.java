package com.itaces.medicata.fps.util;

import lombok.experimental.UtilityClass;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Утилита работы с логами
 */
@UtilityClass
public class LogsUtils {

    /**
     * Маркет
     *
     * @param header    заголовок
     * @param delimeter разделитель
     * @param vars      элементы
     * @return строка лога
     */
    public static String marker(String header, String delimeter, String... vars) {
        final List<String> elements = new ArrayList<>();
        elements.add(header);

        if (vars.length != 0) {
            Arrays.stream(vars).map(var -> var + ":{}").forEach(elements::add);
        }

        return String.join(delimeter, elements);
    }

    /**
     * Строка
     *
     * @param header заголовок
     * @param vars   элементы
     * @return строка лога
     */
    public static String markerLine(String header, String... vars) {
        return marker(header, "\n\t", vars);
    }

    /**
     * Строка
     *
     * @param header заголовок
     * @param vars   элементы
     * @return строка лога
     */
    public static String markerSpace(String header, String... vars) {
        return marker(header, " ", vars);
    }
}
