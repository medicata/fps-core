package com.itaces.medicata.fps;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


/**
 * Spring-boot приложение
 */
@SpringBootApplication
public class FpsApplication {
    /**
     * Main-метод приложения
     *
     * @param args - входные аргументы
     */
    public static void main(String[] args) {
        SpringApplication.run(FpsApplication.class, args);
    }
}
