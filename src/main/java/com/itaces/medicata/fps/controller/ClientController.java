package com.itaces.medicata.fps.controller;

/**
 * Базовый класс контроллеров для API пациентов
 */
public abstract class ClientController extends BasicRestController {
    public static final String root = BasicRestController.root + "/client";
}
