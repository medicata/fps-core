package com.itaces.medicata.fps.controller.admin;

import com.itaces.medicata.fps.controller.BasicRestController;

/**
 * Базовый контроллер для API админки
 */
public abstract class AdminController extends BasicRestController {
    public static final String root = BasicRestController.root + "/admin";
    public static final String PATH_CREATE = "/create";
    public static final String PATH_EDIT = "/edit/{id}";
    public static final String PATH_LIST = "/list";
}
