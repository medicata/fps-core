package com.itaces.medicata.fps.controller.admin;


import com.itaces.medicata.fps.controller.BasicRestController;
import com.itaces.medicata.fps.security.AdminAccount;
import com.itaces.medicata.fps.security.AdminAccountAuthenticationFilter;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.util.Optional;

/**
 * Административный контроллер аутентификации
 */
@Slf4j
@RequestMapping(AdminAuthController.root)
@RestController
public class AdminAuthController extends BasicRestController {
    public static final String root = AdminController.root + "/auth";

    public static final String pathLogin = root + PATH_LOGIN;
    public static final String pathWhoami = root + PATH_WHOAMI;

    /**
     * Логин
     *
     * @param session сессия
     * @return - результат
     */
    @ApiImplicitParams({
            @ApiImplicitParam(name = AdminAccountAuthenticationFilter.SPRING_SECURITY_FORM_USERNAME_KEY, paramType = "query", required = true),
            @ApiImplicitParam(name = AdminAccountAuthenticationFilter.SPRING_SECURITY_FORM_PASSWORD_KEY, paramType = "query", required = true)
    })

    @GetMapping(PATH_LOGIN)
    public Object adminLogin(HttpSession session) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (AdminAccount.class.isAssignableFrom(authentication.getPrincipal().getClass())) {
            ((AdminAccount) authentication.getPrincipal()).setAccessToken(session.getId());
        }
        return authentication.getPrincipal();
    }

    /**
     * Логаут
     */
    @PostMapping(PATH_LOGOUT)
    public void logout() {
        log.info(METHOD_LOGOUT);
    }

    /**
     * Данные пользователя
     *
     * @return - данные пользователя
     */
    @GetMapping(PATH_WHOAMI)
    public Object whoami() {
        return Optional.of(SecurityContextHolder.getContext())
                .map(SecurityContext::getAuthentication)
                .map(Authentication::getPrincipal)
                .orElse(null);
    }

}
