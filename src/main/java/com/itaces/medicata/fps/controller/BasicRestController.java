package com.itaces.medicata.fps.controller;

/**
 * Базовый класс для контроллеров
 */
public abstract class BasicRestController {
    public static final String root = "";

    public static final String PARAM_OBJECT_ID = "object-id";

    public static final String METHOD_CREATE = "create";
    public static final String METHOD_DELETE = "delete";
    public static final String METHOD_EDIT = "edit";
    public static final String METHOD_FILTER = "filter";
    public static final String METHOD_GET = "get";
    public static final String METHOD_PAGE = "page";
    public static final String METHOD_LOGIN = "login";
    public static final String METHOD_LOGOUT = "logout";
    public static final String METHOD_WHOAMI = "whoami";

    public static final String PATH_OBJECT_ID = "/{" + PARAM_OBJECT_ID + "}";
    public static final String PATH_FILTER = "/" + METHOD_FILTER;
    public static final String PATH_LOGIN = "/" + METHOD_LOGIN;
    public static final String PATH_LOGOUT = "/" + METHOD_LOGOUT;
    public static final String PATH_WHOAMI = "/" + METHOD_WHOAMI;
}
