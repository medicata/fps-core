package com.itaces.medicata.fps.controller;


import com.itaces.medicata.fps.config.HttpSessionConfig;
import com.itaces.medicata.fps.security.UserAccountDetails;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.util.Optional;

/**
 * REST-контроллер аутентификации
 */
@Slf4j
@RequestMapping(AuthController.root)
@RestController
public class AuthController extends ClientController {
    public static final String root = ClientController.root + "/auth";

    public static final String pathLogin = root + PATH_LOGIN;
    public static final String pathLogout = root + PATH_LOGOUT;
    public static final String pathWhoami = root + PATH_WHOAMI;

    /**
     * Логин
     *
     * @param session сессия
     * @return - результат
     */
    @ApiImplicitParams({
            @ApiImplicitParam(name = HttpSessionConfig.HEADER_X_CLIENT_TOKEN, paramType = "header", required = false, value = "Токен доступа")
    })
    @GetMapping(PATH_LOGIN)
    public Object login(HttpSession session) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (UserAccountDetails.class.isAssignableFrom(authentication.getPrincipal().getClass())) {
            UserAccountDetails userDetails = (UserAccountDetails) authentication.getPrincipal();
            userDetails.setAccessToken(session.getId());
        }
        return authentication.getPrincipal();
    }

    /**
     * Логаут
     */
    @PostMapping(PATH_LOGOUT)
    public void logout() {
        log.info(METHOD_LOGOUT);
    }

    /**
     * Данные пользователя
     *
     * @return - данные пользователя
     */
    @GetMapping(PATH_WHOAMI)
    public Object whoami() {
        return Optional.of(SecurityContextHolder.getContext())
                .map(SecurityContext::getAuthentication)
                .map(Authentication::getPrincipal)
                .orElse(null);
    }
}
