package com.itaces.medicata.fps.security;

import com.itaces.medicata.fps.error.FpsAuthException;
import liquibase.util.StringUtils;
import lombok.ToString;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.security.authentication.AbstractAuthenticationToken;

/**
 * токен пользователя
 */
@ToString
public class UserAccountAuthenticationToken extends AbstractAuthenticationToken {
    private static final long serialVersionUID = 6759636661056929020L;
    private String authToken;

    protected UserAccountDetails authenticatedUserDetails;

    /**
     * Конструктор
     *
     * @param authenticatedUserDetails - данные пользователя
     */
    public UserAccountAuthenticationToken(UserAccountDetails authenticatedUserDetails) {
        super(authenticatedUserDetails.getAuthorities());
    }


    /**
     * Конструктор
     * @param authToken токен доступа
     */
    public UserAccountAuthenticationToken(String authToken) {
        super(null);
        if(StringUtils.isEmpty(authToken)){
            throw new FpsAuthException("Токен доступа не может быть пустым");
        }
        this.authToken = authToken;
        setAuthenticated(false);
    }

    @Override
    public Object getCredentials() {
        return authToken;
    }

    @Override
    public Object getDetails() {
        return super.getDetails();
    }

    @Override
    public Object getPrincipal() {
        return ObjectUtils.firstNonNull(authenticatedUserDetails, authToken);
    }


    @Override
    public void setAuthenticated(boolean isAuthenticated) throws IllegalArgumentException {
        if (isAuthenticated) {
            throw new IllegalArgumentException(
                    "Cannot set this token to trusted - use constructor which takes a GrantedAuthority list instead");
        }
        super.setAuthenticated(false);
    }
}
