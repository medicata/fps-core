package com.itaces.medicata.fps.security;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.itaces.medicata.fps.config.HttpSessionConfig;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.time.ZonedDateTime;
import java.util.Collection;
import java.util.UUID;

/**
 * Данные аутентифицированного пользователя для механизма аутентфикации Spring
 */
@ToString
@Setter
@Getter
public class UserAccountDetails extends User {
    private static final long serialVersionUID = -8824511504264217883L;

    /**
     * ID
     */
    private UUID id;

    /**
     * Дата и время последнего логина
     */
    private ZonedDateTime lastAccessDateTime;

    /**
     * Текущий access-token
     */
    @JsonProperty(HttpSessionConfig.HEADER_X_AUTH_TOKEN)
    private String accessToken;


    /**
     * Конструктор
     *
     * @param id          = идентификатор
     * @param authorities - роли
     */
    public UserAccountDetails(UUID id, Collection<? extends GrantedAuthority> authorities) {
        super(id.toString(), "", authorities);
        this.id = id;
    }

}
