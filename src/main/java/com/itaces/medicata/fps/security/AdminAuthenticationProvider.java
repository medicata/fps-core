package com.itaces.medicata.fps.security;

import com.itaces.medicata.fps.config.WebSecurityConfig;
import com.itaces.medicata.fps.util.LogsUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Провайдер
 */
@Component
@Slf4j
public class AdminAuthenticationProvider implements AuthenticationProvider {

    @Value("${application.admin.login}")
    private String login;

    @Value("${application.admin.pass}")
    private String pass;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        try {
            if (authentication instanceof UsernamePasswordAuthenticationToken) {
                return this.authenticateSimple((UsernamePasswordAuthenticationToken) authentication);
            }
            else {
                throw new IllegalArgumentException("Unknown authentication type");
            }
        }
        finally {
            log.info(LogsUtils.markerLine("authenticate", "authentication"), authentication);
        }

    }

    private Authentication authenticateSimple(UsernamePasswordAuthenticationToken authentication) {

        // Формирование допустимых ролей для пользователя
        final List<GrantedAuthority> grantedAuthorities = new ArrayList<>();
        if (authentication.getPrincipal().equals(login) && authentication.getCredentials().equals(pass)) {
            grantedAuthorities.add(new SimpleGrantedAuthority("ROLE_" + WebSecurityConfig.FPS_ADMIN));
        }
        AdminAccount userDetails = new AdminAccount(authentication.getPrincipal().toString(),
                                                    authentication.getCredentials().toString(),
                                                    grantedAuthorities);
        return new AdminAuthenticationToken(userDetails);

    }

    @Override
    public boolean supports(Class<?> authentication) {
        return UsernamePasswordAuthenticationToken.class.isAssignableFrom(authentication);
    }
}
