package com.itaces.medicata.fps.security;


import com.itaces.medicata.fps.service.UserAccountService;
import com.itaces.medicata.fps.util.LogsUtils;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;

/**
 * Провайдер доступа
 */
@Component
@Slf4j
@AllArgsConstructor
public class UserAccountAuthenticationProvider implements AuthenticationProvider {


    @Autowired
    UserAccountService userAccountService;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        log.info(LogsUtils.markerLine("authenticate", "authentication"), authentication);
        if (authentication instanceof UserAccountAuthenticationToken) {
            return this.authenticateToken((UserAccountAuthenticationToken) authentication);
        }
        else {
            throw new IllegalArgumentException("Unknown authentication type");
        }
    }

    private Authentication authenticateToken(UserAccountAuthenticationToken authentication) {
        final String authToken = (String) authentication.getCredentials();

            /*
            Поиск данных пользователя по данным с предыдущей аутентификации
             */
        final UserAccountDetails userDetails = this.userAccountService.authenticateByToken(authToken);

        log.info(LogsUtils.markerLine("authenticateDeviceToken", "userDetails"), userDetails);

        return new UserAccountAuthenticationToken(userDetails);

    }

    @Override
    public boolean supports(Class<?> authentication) {
        return UserAccountAuthenticationToken.class.isAssignableFrom(authentication);
    }



}
