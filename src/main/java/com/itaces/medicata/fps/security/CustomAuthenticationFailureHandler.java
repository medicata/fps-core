package com.itaces.medicata.fps.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

/**
 * Простой обработчик ситуации "попытка аутентификации провалилась", который просто возвращает 418 ошибку
 */
public class CustomAuthenticationFailureHandler implements AuthenticationFailureHandler {
    private ObjectMapper objectMapper = new ObjectMapper();

    @Override
    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception) throws IOException, ServletException {
            response.setStatus(HttpStatus.I_AM_A_TEAPOT.value());
            Map<String, Object> data = new HashMap<>();
            data.put(
                    "status",
                    HttpStatus.I_AM_A_TEAPOT.value());
            data.put(
                    "error",
                    HttpStatus.I_AM_A_TEAPOT.getReasonPhrase());
            data.put(
                    "message",
                    exception.getMessage());
            data.put(
                    "timestamp",
                    ZonedDateTime.now().format(DateTimeFormatter.ISO_DATE_TIME));
            data.put(
                    "path",
                    request.getRequestURI());

            response.getOutputStream()
                    .println(objectMapper.writeValueAsString(data));
            response.setHeader(HttpHeaders.CONTENT_TYPE, "application/json");


    }
}
