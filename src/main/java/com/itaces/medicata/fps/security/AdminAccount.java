package com.itaces.medicata.fps.security;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.itaces.medicata.fps.config.HttpSessionConfig;
import lombok.Getter;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.util.Collection;

/**
 * Администратор
 */
@Getter
@Setter
public class AdminAccount extends User {
    /**
     * Текущий access-token
     */
    @JsonProperty(HttpSessionConfig.HEADER_X_AUTH_TOKEN)
    private String accessToken;

    /**
     * Конструктор
     *
     * @param login       имя пользователя
     * @param pass        - пароль
     * @param authorities - права
     */
    public AdminAccount(String login, String pass, Collection<? extends GrantedAuthority> authorities) {
        super(login, pass, authorities);
    }
}
