package com.itaces.medicata.fps.security;


import com.itaces.medicata.fps.config.HttpSessionConfig;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.util.matcher.RequestMatcher;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.UUID;

/**
 * Фильтр аутентификации
 */
@Slf4j
public class UserAccountAuthenticationFilter extends AbstractAuthenticationProcessingFilter {

    private static final String ALREADY_PROCESSED = "PATIENT_AUTHENTICATION_FILTER_REQUEST_ALREADY_PROCESSED";

    /**
     * Конструктор
     *
     * @param requiresAuthenticationRequestMatcher - матчер запроса
     */
    public UserAccountAuthenticationFilter(RequestMatcher requiresAuthenticationRequestMatcher) {
        super(requiresAuthenticationRequestMatcher);
        setAuthenticationSuccessHandler(new DoNothingAuthenticationSuccessHandler());
        setAuthenticationFailureHandler(new CustomAuthenticationFailureHandler());
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException {

        if (request.getAttribute(ALREADY_PROCESSED) != null) {
            return SecurityContextHolder.getContext().getAuthentication();
        }

        request.setAttribute(ALREADY_PROCESSED, UUID.randomUUID().toString());

        log.info("method: {}", request.getMethod());

        String authToken = request.getHeader(HttpSessionConfig.HEADER_X_CLIENT_TOKEN);

        log.info("authToken: {}", authToken);
        if (StringUtils.isNotBlank(authToken)) {
            return this.getAuthenticationManager()
                    .authenticate(new UserAccountAuthenticationToken(authToken));
        }
        return null;
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication authResult) throws IOException, ServletException {
        super.successfulAuthentication(request, response, chain, authResult);
        chain.doFilter(request, response);
    }

    /*@Override
    protected void unsuccessfulAuthentication(HttpServletRequest request, HttpServletResponse response, AuthenticationException failed) throws IOException, ServletException {
        //super.unsuccessfulAuthentication(request, response, failed);
        if(!(failed instanceof FpsAuthenticationException)) {
            SecurityContextHolder.clearContext();
        }
        if (this.logger.isDebugEnabled()) {
            this.logger.debug("Authentication request failed: " + failed.toString(), failed);
            this.logger.debug("Updated SecurityContextHolder to contain null Authentication");
            this.logger.debug("Delegating to authentication failure handler " + super.getFailureHandler());
        }

        super.getRememberMeServices().loginFail(request, response);
        super.getFailureHandler().onAuthenticationFailure(request, response, failed);
    }*/
}
