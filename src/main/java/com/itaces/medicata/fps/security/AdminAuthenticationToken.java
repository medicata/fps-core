package com.itaces.medicata.fps.security;

import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.userdetails.User;

/**
 * Токен
 */
public class AdminAuthenticationToken extends AbstractAuthenticationToken {

    private final User authenticatedUserDetails;

    /**
     * Конструктор
     *
     * @param authenticatedUserDetails - данные пользователя
     */
    public AdminAuthenticationToken(User authenticatedUserDetails) {
        super(authenticatedUserDetails.getAuthorities());
        this.authenticatedUserDetails = authenticatedUserDetails;
        super.setAuthenticated(true);
    }

    @Override
    public Object getCredentials() {
        return authenticatedUserDetails.getUsername();
    }

    @Override
    public Object getDetails() {
        return super.getDetails();
    }

    @Override
    public Object getPrincipal() {
        return authenticatedUserDetails;
    }
}
