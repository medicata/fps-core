package com.itaces.medicata.fps.repository;

import com.itaces.medicata.fps.model.UserAccount;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

/**
 * Репозиторий для пользователей
 */
@Repository
public interface UserAccountRepository extends JpaRepository<UserAccount, UUID> {
    /**
     * Получить пользователя по его идентифкатору
     *
     * @param id - идентификатор
     * @return - пользователь
     */
    UserAccount getById(UUID id);

    /**
     * Поиск пользователя по токену
     * @param token токен
     * @return пользователь
     */
    UserAccount findByAccessToken(String token);

}
