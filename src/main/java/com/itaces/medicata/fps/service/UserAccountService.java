package com.itaces.medicata.fps.service;


import com.itaces.medicata.fps.security.UserAccountDetails;
import org.springframework.validation.annotation.Validated;

/**
 * Сервис пользователей
 */
@Validated
public interface UserAccountService {

    /**
     * Восстановление аутентификации
     *
     * @param authToken - токен доступа
     * @return - данные пользователя
     */
    UserAccountDetails authenticateByToken(String authToken);


}
