package com.itaces.medicata.fps.service.impl;


import com.itaces.medicata.fps.config.WebSecurityConfig;
import com.itaces.medicata.fps.error.FpsAuthException;
import com.itaces.medicata.fps.model.UserAccount;
import com.itaces.medicata.fps.repository.UserAccountRepository;
import com.itaces.medicata.fps.security.UserAccountDetails;
import com.itaces.medicata.fps.service.UserAccountService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * Сервис пользователей
 */
@Slf4j
@Transactional(propagation = Propagation.REQUIRES_NEW)
@Service
public class UserAccountServiceImpl implements UserAccountService {

    @Autowired
    UserAccountRepository accountRepository;

    @Override
    public UserAccountDetails authenticateByToken(String acessToken) {
        // Поиск устройства по его ID и refresh-token
        UserAccount user = accountRepository.findByAccessToken(acessToken);

        if(user == null){
            throw new FpsAuthException("Ошибка идентификации пользователя");
        }

        return this.generateUserDetails(user);
    }

    /**
     * Генерация детайл пользователя
     * @param user пользователь
     * @return детейл
     */
    public UserAccountDetails generateUserDetails(UserAccount user) {
        //final UserAccount user = this.repository.getById(device.getUser().getId());

        // Формирование допустимых ролей для пользователя
        final List<GrantedAuthority> grantedAuthorities = new ArrayList<>();

        // Базовая роль
        grantedAuthorities.add(new SimpleGrantedAuthority("ROLE_" + WebSecurityConfig.FPS_USER));

        return new UserAccountDetails(user.getId(),grantedAuthorities);
    }

}
