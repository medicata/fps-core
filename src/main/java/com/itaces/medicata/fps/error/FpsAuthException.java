package com.itaces.medicata.fps.error;

import org.springframework.security.core.AuthenticationException;


/**
 * Кастомный эксепшен
 */
public class FpsAuthException  extends AuthenticationException {
    /**
     * Конструктор
     * @param msg сообщение
     * @param t ошибка
     */
    public FpsAuthException(String msg, Throwable t) {
        super(msg, t);
    }

    /**
     * Конструктор
     * @param msg сообщение
     */
    public FpsAuthException(String msg) {
        super(msg);
    }
}
