package com.itaces.medicata.fps;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Data;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.testcontainers.containers.PostgreSQLContainer;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Map;

@RunWith(SpringRunner.class)
@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT
)
@ActiveProfiles("ci")
@AutoConfigureMockMvc
@ContextConfiguration(initializers = {TestParent.Initializer.class})
public abstract class TestParent {


    static class Initializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {
        public void initialize(ConfigurableApplicationContext configurableApplicationContext) {
            TestPropertyValues.of(
                    "spring.datasource.url=" + postgres.getJdbcUrl(),
                    "spring.datasource.username=" + postgres.getUsername(),
                    "spring.datasource.password=" + postgres.getPassword()
            ).applyTo(configurableApplicationContext.getEnvironment());
        }
    }

    @Data
    public static class LoginResult {
        private String authToken;
        private String refreshToken;
    }

    //@ClassRule
    public static ObjectMapper objectMapperTest;

    /*GenericContainer coordinatornew GenericContainer<>(DOCKER_IMAGE_NAME)
.withCreateContainerCmdModifier({ cmd -> cmd.withName('coordinator') })
    * */
    public static final String TESTS_OUTPUT_DIR = "tests-output";
    //@ClassRule
    private static PostgreSQLContainer postgres = null;

    static {
        objectMapperTest = Jackson2ObjectMapperBuilder.json().build();
    }

    @Autowired
    protected MockMvc mvc;

    public static File getFile(String name) {
        try {
            Files.createDirectories(Paths.get(TESTS_OUTPUT_DIR));
            return Paths.get(TESTS_OUTPUT_DIR, name).toFile();
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static boolean isWindows() {
        return System.getProperty("os.name").toLowerCase().contains("windows");
    }

    public static String objectToJson(Object object) throws JsonProcessingException {
        return objectMapperTest.writerWithDefaultPrettyPrinter().writeValueAsString(object);
    }

    public static <T> T jsonToObject(String str, Class<T> clazz) throws IOException {
        return (T)objectMapperTest.readValue(str,clazz);
    }

    @BeforeClass
    public static void setContainers() {
        if (postgres == null) {
            postgres = new PostgreSQLContainer("mdillon/postgis:9.6-alpine")
                    .withDatabaseName("patient_api_dev")
                    .withUsername("medicata")
                    .withPassword("medicata");
            postgres.start();
        }
    }

    public MultiValueMap<String, String> objectToMap(Object obj) {
        MultiValueMap parameters = new LinkedMultiValueMap<String, String>();
        Map<String, String> maps = objectMapperTest.convertValue(obj, new TypeReference<Map<String, String>>() {
        });
        parameters.setAll(maps);

        return parameters;
    }

    @Before
    public void init() {
        this.deleteAll();
    }

    private void deleteAll() {

    }

/*
    protected LoginResult loginMockAdmin(UUID deviceId) throws Exception {
        final MockHttpServletResponse response = this.mvc.perform(MockMvcRequestBuilders.get(AdminAuthController.pathLogin)
                                                                          .header(UserAccountAuthenticationFilter.X_PATIENT_DEVICE_ID,
                                                                                  deviceId.toString())
                                                                          .param(AdminAccountAuthenticationFilter.SPRING_SECURITY_FORM_PASSWORD_KEY,
                                                                                 "admin")
                                                                          .param(AdminAccountAuthenticationFilter.SPRING_SECURITY_FORM_USERNAME_KEY,
                                                                                 "admin"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk())
                .andReturn()
                .getResponse();

        Map<String, String> resultMap = objectMapperTest.readValue(response.getContentAsString(), HashMap.class);

        return new LoginResult().setAuthToken(resultMap.get(UserAccountAuthenticationFilter.X_AUTH_TOKEN))
                .setRefreshToken(resultMap.get(UserAccountAuthenticationFilter.X_REFRESH_TOKEN));

    }

    protected LoginResult loginMockUser(UUID deviceId) throws Exception {
        final MockHttpServletResponse response = this.mvc.perform(MockMvcRequestBuilders.get(AuthController.pathLogin)
                                                                          .header(UserAccountAuthenticationFilter.X_PATIENT_DEVICE_ID,
                                                                                  deviceId.toString()))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk())
                .andReturn()
                .getResponse();

        Map<String, String> resultMap = objectMapperTest.readValue(response.getContentAsString(), HashMap.class);

        return new LoginResult().setAuthToken(resultMap.get(UserAccountAuthenticationFilter.X_AUTH_TOKEN))
                .setRefreshToken(resultMap.get(UserAccountAuthenticationFilter.X_REFRESH_TOKEN));
    }


    protected LoginResult loginMockRefreshToken(UUID deviceId, String refreshToken) throws Exception {
        final MockHttpServletResponse response = this.mvc.perform(MockMvcRequestBuilders.get(AuthController.pathLogin)
                                                                          .header(UserAccountAuthenticationFilter.X_PATIENT_DEVICE_ID,
                                                                                  deviceId.toString())
                                                                          .header(UserAccountAuthenticationFilter.X_REFRESH_TOKEN,
                                                                                  refreshToken))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk())
                .andReturn()
                .getResponse();

        Map<String, String> resultMap = objectMapperTest.readValue(response.getContentAsString(), HashMap.class);

        return new LoginResult().setAuthToken(resultMap.get(UserAccountAuthenticationFilter.X_AUTH_TOKEN))
                .setRefreshToken(resultMap.get(UserAccountAuthenticationFilter.X_REFRESH_TOKEN));
    }

    protected void logoutMock(LoginResult loginResult) throws Exception {
        this.logoutMock(loginResult.getAuthToken());
    }

    protected void logoutMock(String xAuthToken) throws Exception {
        this.mvc.perform(MockMvcRequestBuilders.post(AuthController.pathLogout)
                                 .header(UserAccountAuthenticationFilter.X_AUTH_TOKEN, xAuthToken))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk())
                .andReturn()
                .getResponse();
    }

    protected MockHttpServletRequestBuilder makePatientDelete(String path, String authToken) {
        return MockMvcRequestBuilders.delete(path)
                .header(UserAccountAuthenticationFilter.X_AUTH_TOKEN, authToken);
    }

    protected MockHttpServletRequestBuilder makePatientGet(String path, String authToken) {
        return MockMvcRequestBuilders.get(path)
                .header(UserAccountAuthenticationFilter.X_AUTH_TOKEN, authToken);
    }

    protected MockHttpServletRequestBuilder makePatientPatch(String path, String authToken) {
        return MockMvcRequestBuilders.patch(path)
                .header(UserAccountAuthenticationFilter.X_AUTH_TOKEN, authToken);
    }

    protected MockHttpServletRequestBuilder makePatientPost(String path, String authToken) {
        return MockMvcRequestBuilders.post(path)
                .header(UserAccountAuthenticationFilter.X_AUTH_TOKEN, authToken);
    }

    protected void whoamiMock(LoginResult loginResult) throws Exception {
        this.whoamiMock(loginResult.getAuthToken());
    }

    protected void whoamiMock(String xAuthToken) throws Exception {
        this.mvc.perform(MockMvcRequestBuilders.get(AuthController.pathWhoami)
                                 .header(UserAccountAuthenticationFilter.X_AUTH_TOKEN, xAuthToken))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk())
                .andReturn()
                .getResponse();

    }*/
}
